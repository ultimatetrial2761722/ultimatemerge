import re
import os
import requests

# Set the environmental variables
os.environ['GITLAB_ACCESS_TOKEN'] = 'glpat-Gmpcv_NjgsPyG6ru8cHy'
os.environ['GITLAB_PROJECT_ID'] = '46457775'
os.environ['TARGET_BRANCH'] = 'main'

# GitLab API endpoint URLs
BASE_URL = 'https://gitlab.com'
ACCESS_TOKEN = os.environ.get('GITLAB_ACCESS_TOKEN')
SELECTED_PROJECT_ID = os.environ.get('GITLAB_PROJECT_ID')
target_branch = os.environ.get('TARGET_BRANCH')

# Get the branches for a project and sort them by commit date and time
def get_branches(selected_project_id):
    global ACCESS_TOKEN
    url = f'https://gitlab.com/api/v4/projects/{selected_project_id}/repository/branches'

    headers = {
        'Authorization': f'Bearer {ACCESS_TOKEN}'
    }

    try:
        response = requests.get(url, headers=headers)
        response.raise_for_status()

        branches = response.json()
        if branches:
            # Sort branches based on the commit date and time (latest first)
            sorted_branches = sorted(branches, key=lambda branch: (branch['commit']['committed_date'], extract_time(branch['commit']['committed_date'])), reverse=True)

            # Filter out the "main" branch if it has the latest commit
            if sorted_branches[0]['name'] == 'main':
                sorted_branches = sorted_branches[1:]

            return [branch['name'] for branch in sorted_branches]

        print("No branches found.")
    except requests.exceptions.RequestException as e:
        print(f"An error occurred during the API request: {e}")

    return []


# Extract the time portion from the committed_date
def extract_time(committed_date):
    return committed_date.split('T')[1]

# Get the URL of the open merge request for the source branch
def get_merge_request_url(source_branch):
    global ACCESS_TOKEN, SELECTED_PROJECT_ID
    url = f'https://gitlab.com/api/v4/projects/{SELECTED_PROJECT_ID}/merge_requests'

    params = {
        'source_branch': source_branch,
        'state': 'opened'
    }
    headers = {
        'Authorization': f'Bearer {ACCESS_TOKEN}'
    }

    try:
        response = requests.get(url, params=params, headers=headers)
        response.raise_for_status()  # Raise an exception for 4xx or 5xx status codes

        merge_requests = response.json()
        if merge_requests:
            merge_request_url = merge_requests[0]['web_url']
            return merge_request_url

    except requests.exceptions.RequestException as e:
        print(f"An error occurred during the API request: {e}")

    return None

# Extract the merge request ID from the URL
def extract_merge_request_id(merge_request_url):
    match = re.search(r'\/merge_requests\/(\d+)', merge_request_url)
    if match:
        merge_request_id = match.group(1)
        return merge_request_id
    return None

def merge_existing_merge_request(source_branch):
    global ACCESS_TOKEN, SELECTED_PROJECT_ID
    merge_request_url = get_merge_request_url(source_branch)
    if merge_request_url is not None:
        merge_request_id = extract_merge_request_id(merge_request_url)
        if merge_request_id is not None:
            merge_url = f'https://gitlab.com/api/v4/projects/{SELECTED_PROJECT_ID}/merge_requests/{merge_request_id}/merge'

            headers = {
                'Authorization': f'Bearer {ACCESS_TOKEN}'
            }

            try:
                merge_response = requests.put(merge_url, headers=headers)
                merge_response.raise_for_status()

                print(f'Merge request number {merge_request_id} merged successfully')
                return True
            except requests.exceptions.RequestException as e:
                print(f'An error occurred during the API request: {e}')
                return False
    print('No open merge request found')
    return False

# Create a new merge request
def create_merge_request(source_branch):
    global ACCESS_TOKEN, SELECTED_PROJECT_ID,target_branch
    url = f'https://gitlab.com/api/v4/projects/{SELECTED_PROJECT_ID}/merge_requests'
    headers = {
        'Authorization': f'Bearer {ACCESS_TOKEN}'
    }
    data = {
        'source_branch': source_branch,
        'target_branch': target_branch,
        'title': f'Merge {source_branch} into {target_branch}',
    }
    response = requests.post(url, headers=headers, json=data)
    if response.status_code == 201:
        merge_request = response.json()
        return merge_request['id']
    else:
        print(f'Error occurred while creating the merge request: {response.status_code} {response.text}')
    return None

def main():
    global ACCESS_TOKEN, SELECTED_PROJECT_ID, target_branch

    # Get the branches for the selected project
    branches = get_branches(SELECTED_PROJECT_ID)

    if branches:
        # Select the latest branch with commits
        source_branch = branches[0]

        merge_request_url = get_merge_request_url(source_branch)

        if merge_request_url is not None:
            merge_existing_merge_request(source_branch)
        else:
            merge_request_id = create_merge_request(source_branch)
            if merge_request_id is not None:
                merge_existing_merge_request(source_branch)
            else:
                print("Failed to create a new merge request.")
    else:
        print("No branches found.")

# Run the script
if __name__ == '__main__':
    main()
